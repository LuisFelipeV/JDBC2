/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import aula1308.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario {
    
    String disciplina;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
    
            public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;
        
         String insertTableSQL = "INSERT INTO pessoaJDBC"
                + "(nome,idade,endereco,salario,disciplina,cod,funcaoAP) VALUES"
                + "(?,?,?,?,?,?,?)";
         
          try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setDouble(4, this.getSalario());
            prepareStatement.setString(5, this.getDisciplina());
            prepareStatement.setInt(6, this.getCod());
            prepareStatement.setString(7, this.getFuncaoAP());
            

            prepareStatement.executeUpdate();

            System.out.println("professor inserido");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    
    }
                    public void update(){
  
      Conexao c= new Conexao();
        Connection dbConnection= c.getConexao();
        PreparedStatement prepareStatement = null;
       
        String updateTableSQL ="UPDATE pessoaJDBC SET nome=?, idade=?, endereco=?,salario=?,disciplina=? WHERE cod=?" ;
        
          try{
                prepareStatement = dbConnection.prepareStatement(updateTableSQL);
                
                prepareStatement.setInt(6,this.getCod());
                prepareStatement.setString(1,this.getNome());
                prepareStatement.setInt(2,this.getIdade());
                prepareStatement.setString(3,this.getEndereco());
                prepareStatement.setDouble(4,this.getSalario());
                prepareStatement.setString(5,this.getDisciplina());

                
                prepareStatement.executeUpdate();
                
                System.out.println("update aluno");
            }catch (SQLException e ){
                e.printStackTrace(); //aparecer o erro sql
            }
  
        }
                    
         public void getOne() throws SQLException{
            String selectSQL = "SELECT * FROM pessoaJDBC WHERE cod = ?";
            Conexao c= new Conexao();
            Connection dbConnection= c.getConexao();
            
            PreparedStatement ps;
            try{
                ps= dbConnection.prepareStatement(selectSQL);
                ps.setInt(001, this.cod);
                
                ResultSet rs = ps.executeQuery();
                
                if(rs.next()){
                    this.setSalario(rs.getDouble("salario"));
                    this.setEndereco(rs.getString("endereco"));
                    this.setIdade(rs.getInt("idade"));
                    this.setNome(rs.getString("nome"));
                    this.setDisciplina("disciplina");
                }
                
            }catch(SQLException e){
            e.printStackTrace();
            }
     }
}
