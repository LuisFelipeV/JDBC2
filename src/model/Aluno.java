/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import aula1308.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa {
    
    String semestre;
    String curso;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;
        
         String insertTableSQL = "INSERT INTO pessoaJDBC"
                + "(nome,idade,endereco,semestre,curso,cod,funcaoAP) VALUES"
                + "(?,?,?,?,?,?,?)";
         
          try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setString(4, this.getSemestre());
            prepareStatement.setString(5, this.getCurso());
            prepareStatement.setInt(6, this.getCod());
            prepareStatement.setString(7, this.getFuncaoAP());

            prepareStatement.executeUpdate();

            System.out.println("aluno inserido");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    
    }
    
  public void update(){
  
      Conexao c= new Conexao();
        Connection dbConnection= c.getConexao();
        PreparedStatement prepareStatement = null;
       
        String updateTableSQL ="UPDATE pessoaJDBC SET nome=?, idade=?, endereco=?,curso=?,semestre=? WHERE cod=?" ;
        
          try{
                prepareStatement = dbConnection.prepareStatement(updateTableSQL);
                
                prepareStatement.setInt(6,this.getCod());
                prepareStatement.setString(1,this.getNome());
                prepareStatement.setInt(2,this.getIdade());
                prepareStatement.setString(3,this.getEndereco());
                prepareStatement.setString(4,this.getCurso());
                 prepareStatement.setString(5,this.getSemestre());
                
                prepareStatement.executeUpdate();
                
                System.out.println("update aluno");
            }catch (SQLException e ){
                e.printStackTrace(); //aparecer o erro sql
            }
  
        }
  
       public void getOne() throws SQLException{
            String selectSQL = "SELECT * FROM pessoaJDBC WHERE cod = ?";
            Conexao c= new Conexao();
            Connection dbConnection= c.getConexao();
            
            PreparedStatement ps;
            try{
                ps= dbConnection.prepareStatement(selectSQL);
                ps.setInt(001, this.cod);
                
                ResultSet rs = ps.executeQuery();
                
                if(rs.next()){
                    this.setCurso(rs.getString("curso"));
                    this.setEndereco(rs.getString("endereco"));
                    this.setIdade(rs.getInt("idade"));
                    this.setNome(rs.getString("nome"));
                    this.setSemestre(rs.getString("semestre"));;
                    
                }
                
            }catch(SQLException e){
            e.printStackTrace();
            }
     }
       
 
  
  
    
}
