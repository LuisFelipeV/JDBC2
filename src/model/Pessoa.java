/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import aula1308.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */


public class  Pessoa {
    protected String nome;
    protected int idade;
    protected String endereco;
    protected int cod;
    protected String funcaoAP;

    public String getFuncaoAP() {
        return funcaoAP;
    }

    public void setFuncaoAP(String funcaoAP) {
        this.funcaoAP = funcaoAP;
    }
    
    

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
 

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
      public boolean deletar(){
         Conexao c= new Conexao();
        Connection dbConnection= c.getConexao();
        PreparedStatement prepareStatement = null;
        
        String insertTableSQL ="DELETE FROM pessoaJDBC WHERE cod = ?";
        
        try{
                prepareStatement= dbConnection.prepareStatement(insertTableSQL);               
                prepareStatement.setInt(1,this.cod);
               
                
                prepareStatement.executeUpdate();
                
                System.out.println("Pessoa deletada");
            }catch (SQLException e ){
                e.printStackTrace(); //aparecer o erro sql
            }finally{
                c.desconecta();
        }
        return true;
  }
      
        public static ArrayList<Pessoa> getAll() throws SQLException {
        String selectSQL = "SELECT * FROM pessoaJDBC";
        ArrayList<Pessoa> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                
                Pessoa puxandoBD =  new Pessoa();
                //Pessoa puxandoBD;
                
                if(rs.getString("funcaoAP").equals("Professor")){  //Compara com a tabela funcaoAP do banco para saber se é um professor
                    Professor dadosProfessor = new Professor();
                    dadosProfessor.setSalario(rs.getDouble("salario"));
                    dadosProfessor.setDisciplina(rs.getString("disciplina"));
                    puxandoBD = dadosProfessor;
                }
                
                if(rs.getString("funcaoAP").equals("funAdm")){ //Compara com a tabela funcaoAP do banco para saber se é um funADM
                    FuncAdm dadosFuncionarioADM = new FuncAdm();
                    dadosFuncionarioADM.setFuncao(rs.getString("funcao"));
                    dadosFuncionarioADM.setSetor(rs.getString("setor"));
                    dadosFuncionarioADM.setSalario(rs.getDouble("salario"));
                    puxandoBD = dadosFuncionarioADM;
                    
                }
                            
                  if(rs.getString("funcaoAP").equals("Aluno")){ //Compara com a tabela funcaoAP do banco para saber se é um ALUNO
                   Aluno dadosAluno = new Aluno();
                   dadosAluno.setCurso(rs.getString("curso"));
                   dadosAluno.setSemestre(rs.getString("semestre"));
                   puxandoBD = dadosAluno;
                  }
                puxandoBD.setEndereco(rs.getString("endereco"));// Preenche os dados que todos os tipos de pessoa possuem em comum
                puxandoBD.setNome(rs.getString("nome"));
                puxandoBD.setFuncaoAP(rs.getString("funcaoAP"));
                puxandoBD.setIdade(rs.getInt("idade"));
                puxandoBD.setCod(rs.getInt("cod"));
                
                
                
                lista.add(puxandoBD);
                
            }
            }catch(SQLException x){
        }
        return lista;
    }
      
 
    
}
