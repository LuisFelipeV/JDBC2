/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import aula1308.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario{
    
    String setor;
    String funcao;

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    
        public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;
        
         String insertTableSQL = "INSERT INTO pessoaJDBC"
                + "(nome,idade,endereco,salario,setor,funcao,cod,funcaoAP) VALUES"
                + "(?,?,?,?,?,?,?,?)";
         
          try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, this.getNome());
            prepareStatement.setInt(2, this.getIdade());
            prepareStatement.setString(3, this.getEndereco());
            prepareStatement.setDouble(4, this.getSalario());
            prepareStatement.setString(5, this.getSetor());
            prepareStatement.setString(6, this.getFuncao());
            prepareStatement.setInt(7, this.getCod());
            prepareStatement.setString(8, this.getFuncaoAP());
            
            

            prepareStatement.executeUpdate();

            System.out.println("funAdm inserido");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    
    }
        
          public void update(){
  
      Conexao c= new Conexao();
        Connection dbConnection= c.getConexao();
        PreparedStatement prepareStatement = null;
       
        String updateTableSQL ="UPDATE pessoaJDBC SET nome=?, idade=?, endereco=?,salario=?,setor=?,funcao=? WHERE cod=?" ;
        
          try{
                prepareStatement = dbConnection.prepareStatement(updateTableSQL);
                
                prepareStatement.setInt(7,this.getCod());
                prepareStatement.setString(1,this.getNome());
                prepareStatement.setInt(2,this.getIdade());
                prepareStatement.setString(3,this.getEndereco());
                prepareStatement.setDouble(4,this.getSalario());
                prepareStatement.setString(5,this.getSetor());
                prepareStatement.setString(6,this.getFuncaoAP());
                
                prepareStatement.executeUpdate();
                
                System.out.println("update aluno");
            }catch (SQLException e ){
                e.printStackTrace(); //aparecer o erro sql
            }
  
        }
          
        public void getOne() throws SQLException{
            String selectSQL = "SELECT * FROM pessoaJDBC WHERE cod = ?";
            Conexao c= new Conexao();
            Connection dbConnection= c.getConexao();
            
            PreparedStatement ps;
            try{
                ps= dbConnection.prepareStatement(selectSQL);
                ps.setInt(001, this.cod);
                
                ResultSet rs = ps.executeQuery();
                
                if(rs.next()){
                    this.setSalario(rs.getDouble("salario"));
                    this.setEndereco(rs.getString("endereco"));
                    this.setIdade(rs.getInt("idade"));
                    this.setNome(rs.getString("nome"));
                    this.setFuncaoAP(rs.getString("funcaoAP"));
                    this.setSetor(rs.getString("setor"));
                }
                
            }catch(SQLException e){
            e.printStackTrace();
            }
     }
}
