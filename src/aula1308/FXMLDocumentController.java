/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula1308;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import model.Aluno;
import model.FuncAdm;
import model.Pessoa;
import model.Professor;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nomeTabela;
    @FXML
    private TableColumn<Pessoa, String> enderecoTabela;
    @FXML
    private TableColumn<Pessoa, Integer> idadeTabela;
    @FXML
    private TableColumn<Pessoa, String> funcaoTabela;
    @FXML
    private TableColumn<Pessoa, String> salarioTabela;
    @FXML
    private TableColumn<Pessoa, String> setorTabela;
    @FXML
    private TableColumn<Pessoa, String> cursoTabela;
    @FXML
    private TableColumn<Pessoa, String> disciplinaTabela;
    @FXML
    private TableColumn<Pessoa, String> semestreTabela;
    @FXML
    private TableColumn<Pessoa, String> funcaoAPTabela;

    @FXML
    private Label nome;
    @FXML
    private Label idade;
    @FXML
    private Label endereco;
    @FXML
    private Label curso;
    @FXML
    private Label semestre;
    @FXML
    private Label salario;
    @FXML
    private Label disciplina;
    @FXML
    private Label setor;
    @FXML
    private Label funcao;
    @FXML
    private Label cod;
    //Aluno
    @FXML
    private TextField nomeAluno;

    @FXML
    private TextField enderecoAluno;

    @FXML
    private TextField codPessoa;

    @FXML
    private TextField cursoAluno; //setor

    @FXML
    private TextField semestreAluno;

    @FXML
    private TextField idadeAluno;

    @FXML
    private RadioButton idAluno;

    @FXML
    private RadioButton idProfessor;

    @FXML
    private RadioButton idFun;

    @FXML
    private TextField funcaoAdm;

    @FXML
    ToggleGroup tg = new ToggleGroup();

    
    @FXML
    private Button cadastrar;
    @FXML
    private Button detalhesBT;  
    @FXML
    private Button excluiBT;
        @FXML
    private Button modificaBT;

    private Object root;
    ObservableList<Pessoa> lista;

    /* ObservableList<Pessoa> colocarDentroDaList() {

        Aluno al = new Aluno();
        al.setCurso("DS");
        al.setEndereco("Esteio");
        al.setIdade(17);
        al.setNome("Luis");
        al.setSemestre("segundo");
        al.setFuncaoAP("Aluno");
        al.setCod(1);
        
        lista.add(al);

        Professor pf = new Professor();
        pf.setDisciplina("Quimica");
        pf.setEndereco("Canoas");
        pf.setIdade(35);
        pf.setNome("Daniela");
        pf.setSalario(10000);
        pf.setCod(2);
        pf.setFuncaoAP("Professor");
        lista.add(pf);

        FuncAdm fc = new FuncAdm();
        fc.setEndereco("Poa");
        fc.setFuncaoAP("Diretor");
        fc.setIdade(45);
        fc.setNome("Marcio");
        fc.setSalario(5000);
        fc.setSetor("Adm");
        fc.setCod(3);
        fc.setFuncaoAP("Diretor");
        lista.add(fc);

        tabela.setItems(FXCollections.observableList(lista));

        return FXCollections.observableList(lista);
    }*/

    @FXML
    private Label label;

    int cont = 0;

    @FXML
    private void aluno(ActionEvent event) {

        nome.setVisible(true);
        idade.setVisible(true);
        endereco.setVisible(true);
        curso.setVisible(true);
        semestre.setVisible(true);
        cod.setVisible(true);
        codPessoa.setVisible(true);

        nomeAluno.setVisible(true);
        idadeAluno.setVisible(true);
        enderecoAluno.setVisible(true);
        cursoAluno.setVisible(true);
        semestreAluno.setVisible(true);
        cadastrar.setVisible(true);

        disciplina.setVisible(false);
        salario.setVisible(false);
        setor.setVisible(false);
        funcao.setVisible(false);
        funcaoAdm.setVisible(false);
        
        detalhesBT.setVisible(true);
        excluiBT.setVisible(true);
        modificaBT.setVisible(true);

        

        cont = 1;

    }

    @FXML
    private void professor(ActionEvent event) {

        nome.setVisible(true);
        idade.setVisible(true);
        endereco.setVisible(true);
        salario.setVisible(true);
        disciplina.setVisible(true);
        cod.setVisible(true);
        codPessoa.setVisible(true);

        nomeAluno.setVisible(true);
        idadeAluno.setVisible(true);
        enderecoAluno.setVisible(true);
        cursoAluno.setVisible(true); // setor
        semestreAluno.setVisible(true);// salario 
        cadastrar.setVisible(true);

        curso.setVisible(false);
        semestre.setVisible(false);
        setor.setVisible(false);
        funcao.setVisible(false);
        funcaoAdm.setVisible(false);
        
        detalhesBT.setVisible(true);
        excluiBT.setVisible(true);
        modificaBT.setVisible(true);

        cont = 2;
    }

    @FXML
    private void funAdm(ActionEvent event) {
        nome.setVisible(true);
        idade.setVisible(true);
        endereco.setVisible(true);
        salario.setVisible(true);
        setor.setVisible(true);
        funcao.setVisible(true);
        cod.setVisible(true);
        codPessoa.setVisible(true);

        nomeAluno.setVisible(true);
        idadeAluno.setVisible(true);
        enderecoAluno.setVisible(true);
        cursoAluno.setVisible(true); // disciplina
        semestreAluno.setVisible(true);// salario 
        cadastrar.setVisible(true);
        funcaoAdm.setVisible(true);

        curso.setVisible(false);
        semestre.setVisible(false);
        disciplina.setVisible(false);
        
        detalhesBT.setVisible(true);
        excluiBT.setVisible(true);
        modificaBT.setVisible(true);
        
        funcaoTabela.setVisible(true);

        cont = 3;
    }

    @FXML
    private void detalhes(ActionEvent event) {

        if (cont == 1) {
            cursoTabela.setVisible(true);
            semestreTabela.setVisible(true);
            funcaoAPTabela.setVisible(true);

            disciplinaTabela.setVisible(false);
            salarioTabela.setVisible(false);

            setorTabela.setVisible(false);
            salarioTabela.setVisible(false);
        }

        if (cont == 2) {
            disciplinaTabela.setVisible(true);
            salarioTabela.setVisible(true);
            funcaoAPTabela.setVisible(true);

            setorTabela.setVisible(false);

            cursoTabela.setVisible(false);
            semestreTabela.setVisible(false);

        }

        if (cont == 3) {
            setorTabela.setVisible(true);
            salarioTabela.setVisible(true);
            funcaoAPTabela.setVisible(true);

            cursoTabela.setVisible(false);
            semestreTabela.setVisible(false);

            disciplinaTabela.setVisible(false);

        }

    }

    @FXML
    private void excluir(ActionEvent event) {
        Pessoa p = tabela.getSelectionModel().getSelectedItem();
        if (p != null) {
            tabela.getItems().remove(p);
           p.deletar();
        }

    }

    @FXML
    private void cadastrar(ActionEvent event) {
        if (cont == 1) {

            Aluno a = new Aluno();

            a.setIdade(Integer.parseInt(idadeAluno.getText()));
            a.setCurso(cursoAluno.getText());
            a.setEndereco(enderecoAluno.getText());
            a.setNome(nomeAluno.getText());
            a.setCod(Integer.parseInt(codPessoa.getText()));
            a.setSemestre(semestreAluno.getText());
            a.setFuncaoAP("Aluno");
            
           a.inserir();
            lista.add(a);
            tabela.setItems(FXCollections.observableList(lista));
            
           
            
        }

        if (cont == 2) {

            Professor p = new Professor();

            p.setNome(nomeAluno.getText());
            p.setEndereco(enderecoAluno.getText());
            p.setIdade(Integer.parseInt(idadeAluno.getText()));
            p.setDisciplina(cursoAluno.getText());
            p.setSalario(Double.parseDouble(semestreAluno.getText()));
            p.setCod(Integer.parseInt(codPessoa.getText()));
            p.setFuncaoAP("Professor");
            p.inserir();
            lista.add(p);
            tabela.setItems(FXCollections.observableList(lista));
        }

        if (cont == 3) {

            FuncAdm f = new FuncAdm();

            f.setNome(nomeAluno.getText());
            f.setEndereco(enderecoAluno.getText());
            f.setIdade(Integer.parseInt(idadeAluno.getText()));
            f.setSalario(Double.parseDouble(semestreAluno.getText()));
            f.setFuncao(funcaoAdm.getText());
            f.setFuncaoAP("funAdm");
            f.setSetor(cursoAluno.getText());
            f.setCod(Integer.parseInt(codPessoa.getText()));
            f.inserir();
            lista.add(f);
            tabela.setItems(FXCollections.observableList(lista));
        }
    }
    
    @FXML
    private void modificar(ActionEvent event) {
        Pessoa p = tabela.getSelectionModel().getSelectedItem();
     
            if(cont==1){
            p.setNome(nomeAluno.getText());
            p.setEndereco(enderecoAluno.getText());
            p.setFuncaoAP(funcaoAdm.getText());
            p.setIdade(Integer.parseInt(idadeAluno.getText()));
            cont=0;
            }
             if(cont==2){
            p.setNome(nomeAluno.getText());
            p.setEndereco(enderecoAluno.getText());
            p.setFuncaoAP(funcaoAdm.getText());
            p.setIdade(Integer.parseInt(idadeAluno.getText()));
            cont=0;
            
            }
             if(cont==3){
            p.setNome(nomeAluno.getText());
            p.setEndereco(enderecoAluno.getText());
            p.setFuncaoAP(funcaoAdm.getText());
            p.setIdade(Integer.parseInt(idadeAluno.getText()));
            cont=0;
            
            }
               tabela.refresh();
    
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        idAluno.setToggleGroup(tg);
        idProfessor.setToggleGroup(tg);
        idFun.setToggleGroup(tg);

//        tabela.setItems(colocarDentroDaList());
        
    try {
       lista = tabela.getItems();
      
        nomeTabela.setCellValueFactory(new PropertyValueFactory("nome"));
        enderecoTabela.setCellValueFactory(new PropertyValueFactory("endereco"));
        idadeTabela.setCellValueFactory(new PropertyValueFactory("idade"));
        funcaoTabela.setCellValueFactory(new PropertyValueFactory("funcao"));
        cursoTabela.setCellValueFactory(new PropertyValueFactory("curso"));
        disciplinaTabela.setCellValueFactory(new PropertyValueFactory("disciplina"));
        semestreTabela.setCellValueFactory(new PropertyValueFactory("semestre"));
        salarioTabela.setCellValueFactory(new PropertyValueFactory("salario"));
        setorTabela.setCellValueFactory(new PropertyValueFactory("setor"));
        funcaoAPTabela.setCellValueFactory(new PropertyValueFactory("funcaoAP"));
        
        this.tabela.setItems(lista);
        
        ArrayList<Pessoa> listaP;
        listaP = Pessoa.getAll();
            
        for (Pessoa p : listaP) {
            lista.add(p);
        }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
